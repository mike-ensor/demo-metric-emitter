use std::process::exit;

use log::{debug, error, info};

use demo_metric_emitter::{metric_cycle, setup, setup_metrics};

/**
TODO: Goals for this project
============================
* Want an identity for the application changeable by ENV
* Want to pass in configuration for metrics to get created (dynamic metric creation?)
 **/

const PORT: u16 = 9090;

/// Entry point to binary
fn main() {
    match setup(PORT) {
        Ok(app_config) => {
            let global_config = app_config.global.clone();
            let instance_name = global_config.app_name;
            debug!("Instance Name '{instance_name}'");

            let metrics = if let Some(ref metrics) = app_config.metrics {
                metrics
            } else {
                let config_var = app_config
                    .metric_config
                    .unwrap_or("INCORRECT_CONFIG".to_string());
                error!("No metrics of any type found in config files '{config_var}'. Existing early");
                exit(1);
            };

            setup_metrics(metrics.clone());

            info!("Starting metrics loop");

            // start metrics cycle
            metric_cycle(app_config.clone());
        },
        Err(e) => {
            error!("Failed to setup application: {e}");
        }
    };

    loop {
        // do nothing
    }
}
