use std::env;
use std::path::Path;

use config::{Config, ConfigError, Environment, File};
use log::{info, warn};
use serde_derive::Deserialize;

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct GlobalConfig {
    #[serde(rename = "cycle-frequency")]
    pub cycle_frequency: u64,
    #[serde(rename = "app-name")]
    pub app_name: String,
    #[serde(rename = "app-labels")]
    pub app_labels: Option<Vec<MetricLabel>>,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct Gauge {
    pub name: String,
    pub description: String,
    pub labels: Option<Vec<MetricLabel>>,

    #[serde(rename = "update-frequency-average")]
    pub update_frequency_average: f64,

    #[serde(rename = "low-pct")]
    pub low_pct: f64,

    #[serde(rename = "high-pct")]
    pub high_pct: f64,
    pub min: i16,
    pub max: i16,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct Counter {
    pub name: String,
    pub description: String,
    pub labels: Option<Vec<MetricLabel>>,
    #[serde(rename = "update-frequency-average")]
    pub update_frequency_average: f64,

    #[serde(rename = "reset-at-cycles")]
    pub reset_counter: Option<u64>,
    pub starting: u64,
    pub increment: CounterIncrement,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct CounterIncrement {
    pub amount: f64,
    pub low: f64,
    pub high: f64,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct MetricLabel {
    pub key: String,
    pub value: String,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct Histogram {
    pub name: String,
    pub description: String,
    pub labels: Option<Vec<MetricLabel>>,
    #[serde(rename = "update-frequency-average")]
    pub update_frequency_average: f64,
    #[serde(rename = "min-duration")]
    pub min_duration: u64,
    #[serde(rename = "max-duration")]
    pub max_duration: u64,
    #[serde(rename = "count-min")]
    pub count_min: u64,
    #[serde(rename = "count-max")]
    pub count_max: u64,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct MetricsConfig {
    pub gauges: Option<Vec<Gauge>>,
    pub counters: Option<Vec<Counter>>,
    pub histograms: Option<Vec<Histogram>>,
}

#[derive(Debug, Deserialize, Clone)]
#[allow(unused)]
pub struct AppConfig {
    pub metric_config: Option<String>,
    pub global: GlobalConfig,
    pub metrics: Option<MetricsConfig>,
}

impl AppConfig {
    pub fn new() -> Result<Self, ConfigError> {
        // chooses which config/*-metrics.yaml to use
        let selected_metric = env::var("SELECTED_METRIC_CONFIG").unwrap_or_else(|_| "generic".into());
        let app_config_folder = match env::var("APP_CONFIG_FOLDER") {
            Ok(folder) => {
                folder
            },
            Err(_) => {
                info!("APP_CONFIG_FOLDER not set, using default ./config");
                "./config".to_string()
            }
        };

        let metrics_config_folder = match env::var("METRICS_CONFIG_FOLDER") {
            Ok(folder) => {
                folder
            },
            Err(_) => {
                let metrics_containing_folder = format!("{app_config_folder}/metrics");
                info!("METRICS_CONFIG_FOLDER not set, using default {metrics_containing_folder}");
                metrics_containing_folder
            }
        };

        let full_app_config_name = format!("{app_config_folder}/app-config.yaml");
        let path = Path::new(&full_app_config_name);

        if !path.is_file() {
            warn!("App Config File: {} does not exist at path.", &full_app_config_name);
        }

        let s = Config::builder()
            .add_source(File::with_name(&full_app_config_name))
            .add_source(
                File::with_name(&format!("{metrics_config_folder}/{selected_metric}-metrics"))
                    .required(false),
            )
            .add_source(Environment::with_prefix("APP"))
            .set_override(
                "metric_config",
                format!("{app_config_folder}/{selected_metric}-metrics"),
            )?
            .build()?;
        s.try_deserialize()
    }
}
