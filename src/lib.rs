use std::io::{Error, ErrorKind};
use std::ops::RangeInclusive;
use std::sync::atomic::{AtomicU64, Ordering};
use std::thread;
use std::time::Duration;

use env_logger::Target;
use log::{debug, info, log_enabled, trace, Level, error};
use metrics::{counter, describe_counter, describe_gauge, describe_histogram, gauge, histogram};
use metrics_exporter_prometheus::PrometheusBuilder;
use rand::distributions::Distribution;
use rand::distributions::WeightedIndex;
use rand::prelude::ThreadRng;
use rand::Rng;

use settings::AppConfig;

use crate::settings::{CounterIncrement, Gauge, Histogram, MetricLabel, MetricsConfig};

mod settings;

pub fn metric_cycle(app_config: AppConfig) {
    thread::spawn(move || {
        let atomic_count = AtomicU64::new(1); // start at cycle 1
        let cycle_frequency = app_config.global.cycle_frequency;
        info!("Running at frequency of 1 cycle per {cycle_frequency} seconds");
        if !log_enabled!(Level::Debug) {
            info!("Log level is not set to Debug, more details written to log with Debug. Set the RUST_LOG variable if needed");
        }

        let mut random = rand::thread_rng();
        // loop forever
        loop {
            let curr_cycle = atomic_count.fetch_add(1, Ordering::SeqCst);
            debug!("Cycle {curr_cycle}");
            let metrics = app_config.metrics.clone().unwrap();
            // Loop through Gauges
            // TODO: Move these each into independent threads, convert count to atomic integer
            match metrics.gauges {
                Some(gauges) => {
                    for gauge in gauges {
                        // get the name to share/use
                        let name = gauge.name.clone();
                        // determine if the gauge should be added in this cycle
                        let frequency = gauge.update_frequency_average;
                        if should_run_cycle(frequency, curr_cycle) {
                            // if fraction = 0.0, then should create metric in this cycle
                            // Get a value
                            let value = get_gauge_value(&mut random, gauge);
                            gauge!(name.clone()).set(value);
                            debug!("Gauge ({}) - Value: {}", name.clone(), value);
                        } else {
                            trace!("Gauge ({}) - Skipping Cycle", name.clone());
                        }
                    }
                }
                None => {}
            };
            // Loop through Counters
            match metrics.counters {
                Some(counters) => {
                    for counter in counters {
                        let name = counter.name.clone();
                        let frequency = counter.update_frequency_average;
                        if should_run_cycle(frequency, curr_cycle) {
                            // if fraction = 0.0, then should create metric in this cycle
                            match counter.reset_counter {
                                Some(reset_cycle_value) => {
                                    if (curr_cycle % reset_cycle_value) == 0 {
                                        let starting = counter.starting;
                                        counter!(name.clone()).absolute(starting);
                                        debug!("Counter ({}) - Reset to {starting} 'reset-at-cycles'={reset_cycle_value}", name.clone());
                                        continue; // skip the rest of this loop
                                    }
                                }
                                _ => {}
                            };
                            // Get a value
                            let increment = counter.increment.clone();
                            let amount = get_counter_value(&mut random, increment);
                            counter!(name.clone()).increment(amount);
                            debug!("Counter ({}) - Increment: {:?}", name.clone(), amount);
                        } else {
                            trace!("Counter ({}) - Skipping Cycle", name.clone());
                        }
                    }
                }
                None => {}
            };
            // Loop through Histograms
            match metrics.histograms {
                Some(histograms) => {
                    for histogram in histograms {
                        let name = histogram.name.clone();
                        let frequency = histogram.update_frequency_average;
                        if should_run_cycle(frequency, curr_cycle) {
                            // if fraction = 0.0, then should create metric in this cycle
                            let times: Vec<Duration> =
                                get_histogram_durations(&mut random, histogram);
                            for time_took in times {
                                histogram!(name.clone()).record(time_took);
                                debug!("histogram ({}) - Record: {:?}ms", name.clone(), time_took);
                            }
                        } else {
                            trace!("histogram ({}) - Skipping Cycle", name.clone());
                        }
                    }
                }
                None => {}
            };
            thread::sleep(Duration::from_millis(cycle_frequency * 1000));
        }
    });
}

fn get_histogram_durations(random: &mut ThreadRng, histogram: Histogram) -> Vec<Duration> {
    let min_duration = histogram.min_duration;
    let max_duration = histogram.max_duration;
    let count_min = histogram.count_min;
    let count_max = histogram.count_max;

    let mut response = vec![];
    let count = random.gen_range(count_min..=count_max);
    for _ in count_min..=count {
        let duration = random.gen_range(min_duration..=max_duration);
        response.push(Duration::from_secs(duration));
    }

    response.clone()
}

fn should_run_cycle(frequency: f64, count: u64) -> bool {
    let check: f64 = count as f64 * frequency;
    let fract = check.fract();
    fract == 0.0
}

/// Returns a value for Gauge based on configuration
fn get_gauge_value(random: &mut ThreadRng, gauge: Gauge) -> f64 {
    let low_pct = (gauge.low_pct * 100.0).round() as i64;
    let high_pct = (gauge.high_pct * 100.0).round() as i64;
    let min = gauge.min as u64;
    let max = gauge.max as u64;
    let weights = vec![low_pct, high_pct];
    let value = get_weighed_value(random, weights, min..=max);
    value as f64
}

fn get_counter_value(random: &mut ThreadRng, increment: CounterIncrement) -> u64 {
    let low = increment.low;
    let high = increment.high;
    let amount = increment.amount;
    let random = random.gen_range(low..=high); // must be inclusive since values could be equal
    (random * amount).round() as u64
}

/// This method a random number between a split in the min..max range where one side is weighted more than the other by a percentage
/// Algorithm is as follows:
/// 1. creates buckets based on quantities buckets provided
/// 2. Weights are passed in for the buckets representing the bucket weights
/// 3. WeightedRandom pick between the two sides (index will be the weighted random selection)
/// 4. Use the selected bucket range and pull a random from that range
/// Returns a random number from the algorithm
fn get_weighed_value(random: &mut ThreadRng, weights: Vec<i64>, range: RangeInclusive<u64>) -> u64 {
    let min = range.start().clone();
    let max = range.end().clone();
    let buckets = weights.len() as u64;
    // 1. Create "buckets" split into n-buckets segments
    let bucket_segment_size = (max + min) / buckets;

    let mut buckets_ranges: Vec<(u64, u64)> = vec![];
    for bucket in 0..buckets {
        let start = bucket_segment_size * bucket;
        let end = start + bucket_segment_size;
        buckets_ranges.push((start, end));
    }
    // Select a left-or-right
    let dist = WeightedIndex::new(weights.clone()).unwrap();
    // create "choices" corresponding to the 'buckets'
    let choice_bucket_indexes: Vec<usize> = vec![0, 1]; // could be usize
                                                        // choose one of the buckets
    let weight_index = dist.sample(random);
    let bucket_choice = choice_bucket_indexes[weight_index];
    debug!("Bucket #{bucket_choice} of {buckets} chosen");
    // this will be a challenge (along with 1/2 way)
    let range = buckets_ranges[bucket_choice];
    let result = random.gen_range(range.0..range.1);
    let used_pct = weights[weight_index];
    debug!("Weighted random number {result} chosen from bucket {bucket_choice} with {used_pct}%");
    result
}

/// Initial setup and service creation
pub fn setup(prometheus_port: u16) -> Result<AppConfig,Error> {
    // setup logger
    env_logger::builder().target(Target::Stdout).init();

    match PrometheusBuilder::new()
        .with_http_listener(([0, 0, 0, 0], prometheus_port))
        // TODO: set_buckets - to setup histogram buckets
        .install()
    {
        Ok(_) => info!("Prometheus is up and running on port {prometheus_port}"),
        Err(error) => log::error!("Problem opening the file: {:?}", error),
    };

    let app_config = match AppConfig::new() {
        Ok(app) => app,
        Err(err) => {
            error!("ERROR: {:?}", err);
            return Err(Error::new(ErrorKind::NotFound, err));
        }
    };

    Ok(app_config)
}

pub fn setup_metrics(metrics: MetricsConfig) {
    match metrics.gauges {
        Some(gauges) => {
            for gauge in gauges {
                debug!(
                    "Setting up Gauge ==> Name: {}, description: {}",
                    gauge.name, gauge.description
                );

                let labels: Vec<(String, String)> = get_labels(gauge.labels.clone());
                let name = gauge.name.clone();
                describe_gauge!(name.clone(), gauge.description);
                gauge!(name.clone(), &labels);
            }
        }
        None => {}
    };

    match metrics.counters {
        Some(counters) => {
            for counter in counters {
                debug!(
                    "Setting up Counter ==> Name: {}, description: {}",
                    counter.name, counter.description
                );
                let name = counter.name.clone();
                let labels: Vec<(String, String)> = get_labels(counter.labels.clone());
                let starting = counter.starting;
                describe_counter!(name.clone(), counter.description);
                counter!(name.clone(), &labels);
                counter!(name.clone()).absolute(starting);
            }
        }
        None => {}
    };

    match metrics.histograms {
        Some(histograms) => {
            for histogram in histograms {
                debug!(
                    "Setting up Histogram ==> Name: {}, description: {}",
                    histogram.name, histogram.description
                );
                let labels: Vec<(String, String)> = get_labels(histogram.labels.clone());
                let name = histogram.name.clone();
                describe_histogram!(name.clone(), histogram.description);
                histogram!(name.clone(), &labels);
            }
        }
        None => {}
    };
}

/// Creates a Vector of String,String labels from the configuration of the metrics
fn get_labels(metric_labels: Option<Vec<MetricLabel>>) -> Vec<(String, String)> {
    match metric_labels {
        Some(labels) => {
            let mut use_labels: Vec<(String, String)> = vec![];
            for label in labels {
                let key = label.key;
                let label = label.value;
                use_labels.push((String::from(key), String::from(label)));
            }
            use_labels.clone()
        }
        None => {
            vec![]
        }
    }
}

#[cfg(test)]
mod unit_tests {
    use test_log::test;

    use super::*;

    #[test]
    fn weighted_random_two_bucket() {
        let mut random = rand::thread_rng();
        let low = 0;
        let high = 100;
        let weights = vec![100, 0];
        let result = get_weighed_value(&mut random, weights, low..=high);
        assert!(result <= 50);

        let weights_high = vec![0, 100];
        let result_high = get_weighed_value(&mut random, weights_high, low..=high);
        assert!(result_high >= 50);
    }

    #[test]
    fn weighted_random_four_bucket() {
        let mut random = rand::thread_rng();
        let low = 0;
        let high = 100;
        let weights = vec![100, 0, 0, 0];
        let result = get_weighed_value(&mut random, weights, low..=high);
        debug!("Result: {result}");

        assert!(result <= 25);
    }
}
