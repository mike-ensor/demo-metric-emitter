# Build stage
FROM rust:1.74.1 as builder

WORKDIR /app

COPY . .

RUN cargo build --release

#########################################
######## Run stage ######################
#########################################

FROM rust:1.74.1

WORKDIR /app

# prometheus scraper
EXPOSE 9090

COPY --from=builder /app/target/release/demo-metric-emitter ./

CMD ["/app/demo-metric-emitter"]