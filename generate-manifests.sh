#!/bin/bash

CI_COMMIT_TAG=$1
OUTPUT_DIR="${2:-manifests}"
FILE=".metadata/CURRENT_RELEASED_VERSION"

if [[ -z "$CI_COMMIT_TAG" ]]; then
  echo "Usage: ./generate-manifests.sh <version>"
  exit 1
fi

function update_manifests() {
  echo "Building K8s manifests using latest version"
  # shellcheck disable=SC2190
  METRIC_FILES=( "burger" "manufacturing" "generic" )

  yq eval '.images[0].newTag = strenv(CI_COMMIT_TAG)' -i base/app-base/kustomization.yaml

  for metric in "${METRIC_FILES[@]}"; do
    echo "kustomize build \"base/overlays/${metric}\" > \"${OUTPUT_DIR}/demo-metric-emitter-${metric}.yaml\""
    kustomize build "base/overlays/${metric}" > "${OUTPUT_DIR}/demo-metric-emitter-${metric}.yaml"
  done

  yq eval '.images[0].newTag = "IMAGE_TAG"' -i base/app-base/kustomization.yaml
}

function create_metadata() {
  echo "${CI_COMMIT_TAG}" > "$FILE"
  echo "Updated Released Version: ${CI_COMMIT_TAG}"
}

function print_current_version() {
  VERSION=$(cat "${FILE}")
  echo "Current Released Version: ${VERSION}"
}

if [[ -f "${FILE}" ]]; then
  print_current_version
fi

update_manifests

create_metadata