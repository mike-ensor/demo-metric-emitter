# Metrics

Each of the metrics below are controlled using YAML configuration. While each of the three supported metrics are distinct, 
there are some common fields that are consistent. Additionally, all metrics are emitted in the "metrics cycle", a loop 
that runs on a regularly timed interval.

All of the metrics are randomly generated values based on the configuration.

## Metrics Cycle

Metrics are all emitted using an isolated thread that runs on a programmable cycle frequency. The "config/app-config.yaml" file 
dictates the frequency of the cycle. The default value is 1 second, meaning 1 cycle per second.

Each metric can set the `update-frequency-average` variable which dictates how frequently the metric is emitted. The value is 
a decimal/floating-point number less-than or equal to `1.0`. 

### Common Metric Values

These fields are available (or required) for all of the metric types.

| Name                       | Description                                                                             | Type              | Default | Optional |
|----------------------------|-----------------------------------------------------------------------------------------|-------------------|:-------:|:--------:|
| `name`                     | lower-case, alpha and dash only name                                                    | String            |   N/A   |    N     |
| `description`              | Longer description of the metric. Limited to 255 characters                             | String            |   N/A   |    N     |
| `labels`                   | Key-Value label attributing the metric with Prometheus data model                       | Key/Value Strings |   N/A   |    Y     |
| `update-frequency-averate` | Value used to calculate how frequently the metric should be emitted in the Metric Cycle | Float             |   1.0   |    N     |

### Metric Frequency Cycle Examples

```yaml
# Emit every cycle
update-frequency-average: 1.0 

# Every other cycle
update-frequency-average: 0.5

# Every eight (8) cycle.
update-frequency-average: 0.125
```


## Gauge

Gauge will start at an initialized value (default=0) and increase according to the cycle loop (default 1.0 or every cycle). Counters 
can generate values from a set of weighed buckets. Initially, there are 2 buckets splitting the `min` and `max` in half. Future 
feature will expose the code functionality allowing for more than 2 buckets [see Issue #5](https://gitlab.com/mike-ensor/demo-metric-emitter/-/issues/5). 

| Name       | Description                                               | Type    | Default | Optional |
|------------|-----------------------------------------------------------|---------|:-------:|:--------:|
| `min`      | Lowest value the metric can be                            | Integer |    0    |    N     |
| `max`      | Highest value the metric can be (inclusive)               | Integer |   N/A   |    N     |
| `low-pct`  | Weighted value where % of values are in the lower bucket  | Float   |   0.5   |    N     |
| `high-pct` | Weighted value where % of values are in the higher bucket | Float   |   0.5   |    N     |

### Gauge Configuration Scenarios

#### No weights (or even weights)

This configuration will create 2 buckets, each at 50%. This is the default case and the results will be a random value 
between 0 and 8, with no weights between the upper or lower bucket.

```yaml
  low-pct: 0.50     # of the lower 1/2 bucket. 50% of the time, a value will be between 0 and 3
  high-pct: 0.50    # of the upper 1/2 bucket. 50% of the time, a value will be between 4 and 8
  min: 0            # Lowest value that can be randomly generated
  max: 8            # Highest value that can be randomly generated
```

#### Using Weighted bucket
This configuration will create 2 buckets, one between 0 and 3 and one between 4 and 8. Then 75% of the time, a random number 
will be generated in the lower-bucket, and 25% of the time, the value will be in the upper bucket. Values will always be between 0 and 8 (inclusive).
```yaml
  low-pct: 0.75     # of the lower 1/2 bucket. 75% of the time, a value will be between 0 and 3
  high-pct: 0.25    # of the upper 1/2 bucket. 25% of the time, a value will be between 4 and 8
  min: 0            # Lowest value that can be randomly generated
  max: 8            # Highest value that can be randomly generated
```

## Counter

Counters will start at an initialized value (default=0) and increase according to the cycle loop (default 1.0 or every cycle). 

| Name                | Description                                                                                      | Type    | Default | Optional |
|---------------------|--------------------------------------------------------------------------------------------------|---------|:-------:|:--------:|
| `starting`          | Value to start the counter with (or when reset)                                                  | Integer |    0    |    N     |
| `increment.amount`  | Amount to increase the counter on each cycle                                                     | Integer |    1    |    N     |
| `increment.low`     | Multiplier used to increase when using the low bucket. If 1.0, `increase.amount` will be used    | Float   |   1.0   |    N     |
| `increment.high`    | Multiplier used to increase when using the high bucket. If 1.0, `increase.amount` will be used   | Float   |   1.0   |    N     |
| `reset-at-cycles`   | Resets the counter back to the `starting` value at each n-cycles.                                | Integer |    0    |    Y     |



## Histogram

## Summary (TBD)

## Advanced Settings

### Viewing output for all metrics

Metrics are emitted to the `debug` log level. Set the environment variable `RUST_LOG` to `debug` to see each metric written to the 
`stdout` console.