# Overview

This application produces metrics in Open Telemetry format through an endpoint using [Prometheus data model](https://prometheus.io/docs/concepts/data_model/). This project 
emits 3 of the 4 common [Metric types](https://prometheus.io/docs/concepts/metric_types/): 

* [Counter](https://prometheus.io/docs/concepts/metric_types/#counter)
* [Gauge](https://prometheus.io/docs/concepts/metric_types/#gauge)
* [Histogram](https://prometheus.io/docs/concepts/metric_types/#histogram)
* [Summary](https://prometheus.io/docs/concepts/metric_types/#summary) -- (not supported yet, but soon)

This project uses YAML configuration files to describe the different metrics that should be emitted. Each type of metric and 
documentation are covered 

## Deploying to K8s

Apply the `manifest/demo-metric-emitter.yaml` file to your cluster to test the demonstration. Follow the guide below and in [docs/metrics.md](docs/metrics.md) to 
create your own.

## Metrics

The project uses an ENV variable 'METRIC_CONFIG` to determine what configuration file to use when running. The value of 
`METRIC_CONFIG` is the prefix of the file with `-metrics.yaml` being the suffix.

To change the file, or when using Kubernetes to run, set the ENV `METRIC_CONFIG=generic` for the generic, or use `burgers` as the optional built-in. Create your own too!

> :star: [Detailed list of metrics and their configuration details](docs/metrics.md). 

Make your own setup of Metrics  to simulate your own need  

### The "Metric Cycle" (loop)

The metrics are emitted based on a cycle loop. One cycle per `cycle-frequency` as defined in `${METRIC_CONFIG_FOLDER}/app-config.yaml`. 

METRIC_CONFIG_FOLDER is the ENV variable pointing to the folder which `app-config.yaml`, `*-metrics.yaml` files are located.

## Contribute

Follow the [CONTRIBUTING.md] guide and submit Merge Requests and Issues. Please feel free to +1 or -1 Issues and drop comments