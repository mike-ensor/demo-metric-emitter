#!/bin/bash
VERSION="$1"
APP="${2:-$CONTAINER_NAME}" # default to backup vs restore (lowercase)
DOCKERFILE="Dockerfile"

function build_container() {
  docker build -f "${DOCKERFILE}" -t "${APP}:${VERSION}" .
  if [[ $? -ne 0 ]]; then
      echo "Cannot build docker"
      exit 1
  fi
}

function release_container() {
  docker tag "${APP}:${VERSION}" "${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:${VERSION}"
  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]]; then
      echo "Cannot Tag Docker Build with version"
      exit 1
  fi
  docker tag "${APP}:${VERSION}" "${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:latest"
  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]]; then
      echo "Cannot Tag Docker Build with latest"
      exit 1
  fi
  docker push "${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:${VERSION}"
  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]]; then
      echo "Cannot Push Version Tagged"
      exit 1
  fi
  docker push "${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:latest"
  # shellcheck disable=SC2181
  if [[ $? -ne 0 ]]; then
      echo "Cannot Push Latest Tagged"
      exit 1
  fi
}

######################## Main

# Check if USE_CLOUD is set to anything, if not, ask if it should be
if [[ -z "${USE_CLOUD}" ]]; then
  echo ""
  read -p "Build using gcloud? If No, build and push locally (y/N): " proceed
  if [[ "${proceed}" =~ ^([yY][eE][sS]|[yY])$ ]]; then
    USE_CLOUD=true
  else
    USE_CLOUD=false
  fi
fi

if [[ -z "${APP}" ]]; then
  echo "Usage: ./build.sh <VERSION> <APP-optional>"
  echo "       APP can be automated by setting 'CONTAINER_NAME=<some-name>' in the .env or .envrc file"
  echo "       APP can also be passed in via CLI"
  exit 1
fi

REQUIRED_VARIABLES=( "APP" "VERSION" "REPO_HOST" "REPO_FOLDER" "PROJECT_ID" )
# shellcheck disable=SC2043
HAS_ERROR=0
for REQ in "${REQUIRED_VARIABLES[@]}"
do
  if [[ -z "${REQ}" ]]; then
    echo "Variable ${REQ} is required and does not have a value"
    HAS_ERROR=1
  fi
done

if [[ ${HAS_ERROR} -gt 0 ]]; then
  echo "One or more required variables are not set. Please set and re-run."
  exit 1
fi

echo "Building ${REPO_HOST}/${PROJECT_ID}/${REPO_FOLDER}/${APP}:${VERSION}..."

if [[ ${USE_CLOUD} == true ]]; then
  gcloud builds submit --config=cloudbuild.yaml --substitutions=_REPO_REGION="${REPO_REGION}",_REPO_FOLDER="${REPO_FOLDER}",_CONTAINER_NAME="${CONTAINER_NAME}" .
else
  build_container
  release_container
fi